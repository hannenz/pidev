# Docker containers for Raspberry Pi & Pico development

Provides two images for docker containers which can be used to cross
compile C/C++ for Raspberry Pi and Pico

To use it, first build the images from the Dockerfiles:

```
docker build -t pidev - < Dockerfile.pidev
docker build -t picodev - < Dockerfile.picodev
```

Then cd into a C/C++ project directory and use the image to do the cross-compiling:

## Raspberry Pi Development

```
cd ~/picodev/projects/pi/my-project
docker run -it -v ${PWD}:/workbench -w /workbench/build picodev cmake ..
docker run -it -v ${PWD}:/workbench -w /workbench/build picodev make
docker run -it -v ${PWD}:/workbench -w /workbench/build picodev make install
```



## Raspberry Pico Development

```
cd ~/picodev/projects/pico/my-project
docker run -it -v ${PWD}:/workbench -w /workbench/build picodev cmake ..
docker run -it -v ${PWD}:/workbench -w /workbench/build picodev make
docker run -it -v ${PWD}:/workbench -w /workbench/build picodev make install
```

You might of course alias this to something short:

```
alias pdev='docker run -it -v $PWD:/workbench -w /workbench/build picodev'
```
